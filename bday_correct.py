#!/usr/bin/env python
#coding:utf-8

##############################################################################
#
# Test for BigData
# For this script yuo must config.cfg file and create two file .service and .timer
#
# Written by : Evgeny Melnichenko
# Created date: May 31, 2016
# Last modified: june 06, 2016
# Tested with : Python 2.7.9
# Script Revision: 0.0.3
#
##############################################################################

import ConfigParser
import sys
import json
import requests
import lxml
import datetime
from smtplib import SMTP
from BeautifulSoup import BeautifulSoup
from datetime import date, timedelta as td

conf = ConfigParser.RawConfigParser()
conf.read('/home/melnichenko/py_scripts/conf.cfg')

BASE_URL = conf.get('confluence', 'confluence_connection_string') 
USERNAME = conf.get('confluence', 'confluence_user')
PASSWD = conf.get('confluence', 'confluence_password')
SMTP_SERVER = conf.get('imap', 'imap_server_host_name')

r = requests.get((BASE_URL), auth=(USERNAME, PASSWD))

#Test connect and get data from confluence page
if(r.ok):
#    print 'Connection OK!'
    soup = BeautifulSoup(r.content)
else:
#    print 'Connection FAIL'
    r.raise_for_status()
    sys.exit()

#Create empty list for parse xml
name = []
mail = []
bday = []
cab = []

table = soup.find("table")
for row in table.findAll("tr"):
    cells = row.findAll("td")
    if len(cells) == 12:
       name.append(cells[3].find(text=True))
       mail.append(cells[4].find(text=True))
       bday.append(cells[7].find(text=True))
       cab.append(cells[9].find(text=True))
new_list = zip(name, mail, bday,cab)

#Check birthday in next 7 day
birthday = []
non_birthday = []
fired = u'уволен'
now_day = datetime.date.today()
numdays = 7
datelist = [now_day + datetime.timedelta(days=x) for x in range(0, numdays)]

for i in new_list[1:]:
    p = datetime.datetime.strptime(i[2],'%d.%m').date()
    np = p.replace(year=date.today().year)
    if np in datelist:
        birthday.append(i)
    else:
        if not fired in i:
            non_birthday.append(i)

#Send mail notification
debuglevel = 0
smtp = SMTP()
smtp.connect(SMTP_SERVER,25) 

extract_birthday = zip(*birthday)
extract_non_birthday = zip(*non_birthday)

if birthday:
    birthday_man = [x.encode('UTF8') for x in extract_birthday[0]]
    birthday_date= [x.encode('UTF8') for x in extract_birthday[2]]
    to_addr = [x.encode('UTF8') for x in extract_non_birthday[1]]
    from_addr = 'e.melnichenko@nspk.ru'
    for i in birthday:
        subj = 'День рождения!'
        message_text = 'Уважаемые коллеги!\nВ ближайшее 7 дней день рождение у %s' % ('"'+'", "'.join(birthday_man)+'"')
else:
    sys.exit()
        
msg = 'From: %s\nTo :%s\nSubject: %s\n%s' % (from_addr, to_addr, subj, message_text)
smtp.sendmail(from_addr, to_addr, msg)
smtp.quit()